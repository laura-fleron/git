---
author: Laura Fléron
title: Git pour l'enseignant

tags:
 - Git vu par l'enseignant
---

# Git enseignant 🚀

Git peut être un outil précieux pour les enseignants lors de l'évaluation de projets informatiques réalisés par des étudiants.

Voici les indicateurs que l'enseignant peut chercher à l'aide de Git :

**Gérer le projet en amont** :

   - L'enseignant peut mettre en place un cahier des charges structurel afin de guider les étudiants dans les rôles qui devront assurer dans le projet.

   - **Indicateur** : Les étudiants ont respecté les missions du CDF du projet.

**Accéder au code source** :

   - Git permet à l'enseignant d'accéder facilement au code source du projet des étudiants. Cela facilite l'examen du travail effectué et permet à l'enseignant de mieux comprendre la qualité et la complexité du projet par l'analyse du dépôt.

   - **Indicateurs** : L'accés au code source est visible sur la brance *main*, des commentaires donnent des renseignements sur la fonctionnalités du code.

**Suivre les contributions individuelles** :

   - Grâce à Git, l'enseignant peut suivre les contributions individuelles des étudiants au projet. Il peut voir qui a contribué à quelles parties du code, ce qui est utile pour évaluer la répartition du travail au sein de l'équipe.

   - **Indicateurs** : Le *mur* de chacuns des contributeurs du projet indique l'activité dans le projet.

**voir l'historique du projet** :

   - Git enregistre un historique complet des modifications apportées au code source. L'enseignant peut examiner cet historique *(commit)* pour voir comment le projet a évolué au fil du temps et évaluer la progression des étudiants.

   - **Indicateur** : La page *membres/activité* mentionne les commits et précise les apports de chacun.

**voir les conflits et les résolutions de problèmes** :

   - Si plusieurs étudiants travaillent sur le même projet, Git peut détecter les conflits entre leurs modifications. L'enseignant peut examiner comment ces conflits ont été gérés et résolus, ce qui est un aspect important de la collaboration en développement logiciel.

   - **Indicateur** : Le conflit est documenté, une solution est validée par le responsable du groupe.

**Voir l'évolution du projet au cours du temps** :

   - En examinant l'utilisation de Git, l'enseignant peut évaluer la manière dont les étudiants ont géré leur projet. Cela inclut la planification, la gestion des tâches, la gestion des délais et la communication au sein de l'équipe.

   - **Indicateur** : Des tâches dans la planification sont crées, la *méthode des post-it* montre l'avancement de celles-ci, elles sont toutes terminées en fin de projet.

**Voir la documentation** :

   - Git encourage la documentation du code source. L'enseignant peut évaluer la qualité des commentaires et des descriptions de commit, ce qui peut aider à comprendre le raisonnement derrière les décisions prises par les étudiants.

      * Une documentation peut être faite par ajout de fichier au format *md*.
      * Un wiki est construit.
      * Une documentation plus poussée peut être réalisée si le serveur Git est propulsé par un générateur de site statique comme MkDocs.

   - **Indicateur** : Au moins un fichier readme.md est mis à jour. Le déployement d'une documentation statique est réalisée, un wiki ?

!!! success "En résumé"

    En utilisant Git comme outil de gestion de version, l'enseignant peut avoir une vision claire de la progression et de la qualité du travail des étudiants tout au long du projet. Cela permet une évaluation plus précise et objective des projets informatiques réalisés par les étudiants.
