---
author: Laura Fléron
title: Evaluer
tags:
 - Evaluer avec Git
---

# Evaluer avec Git 🚀

Lors de l'évaluation d'un projet informatique réalisé par des étudiants, les intentions pédagogiques visent généralement à évaluer non seulement les compétences techniques des étudiants, mais aussi leur capacité à appliquer des concepts et des méthodologies appris en classe, ainsi que leur aptitude à travailler en équipe et à résoudre des problèmes. Voici quelques-unes des intentions pédagogiques courantes lors d'une telle évaluation :

**Comprendre des concepts informatiques** :

   - Évaluer la compréhension des concepts informatiques fondamentaux liés au projet, tels que les algorithmes, les structures de données, la programmation, les bases de données, etc.

**Appliquer des compétences techniques** :

   - Vérifier la capacité des étudiants à appliquer leurs compétences techniques pour concevoir, développer et mettre en œuvre une solution logicielle.

**Avoir de la méthode** :

   - Évaluer si les étudiants ont utilisé des méthodologies de développement appropriées, telles que le modèle en cascade, la méthodologie *agile*, en fonction des objectifs du projet.

**Produire un code de qualité** :

   - Evaluer la qualité du code source, y compris la lisibilité, la maintenabilité, la documentation et la conformité aux normes de codage.

**Elaborer un modèle conceptuel** :

   - Examiner la conception globale du système, y compris l'architecture, la modélisation des données, les diagrammes UML (Unified Modeling Language).

**Recourir à des tests** :

   - Évaluer la mise en œuvre de tests, y compris les tests unitaires, les tests d'intégration et les tests d'acceptation, ainsi que la validation des résultats par rapport aux exigences.

**Gérer le projet** :

   - Une gestion de projet efficace est essentielle pour respecter les délais et les ressources allouées. Évaluez la manière dont le projet a été géré à savoir : 

      * la planification doit avoir été posée en amont du projet et mis à jour tout au long de celui-çi.
      * la répartition des tâches doit être clairement établie et de manière équilibrée.
      * le suivi de l'avancement doit être évaluer régulièrement afin de garantir le délai de livraison.

**Collaborer en équipe** :

   - Examiner la capacité des étudiants à travailler en équipe, à communiquer efficacement, à résoudre des conflits et à coordonner leurs efforts.

**Créer et innover** :

   - Encourager les étudiants à apporter des idées créatives et innovantes dans la conception de la solution, si cela est pertinent pour le projet.

**Présenter et communiquer** :

   - Évaluer la capacité des étudiants à présenter leur projet de manière claire et convaincante, à expliquer leurs choix de conception et à répondre aux questions.

**Critiquer** :

   - Encourager les étudiants à réfléchir de manière critique sur les succès, les échecs et les leçons tirées de leur projet, et à proposer des améliorations potentielles.

**Avoir une vision éthique et responsable** :

   - Sensibiliser les étudiants aux questions d'éthique liées au développement logiciel, telles que la sécurité des données, la confidentialité et la propriété intellectuelle.

!!! success "En résumé"

    En structurant l'évaluation d'un projet informatique autour de ces intentions pédagogiques, les enseignants peuvent évaluer de manière globale le projet. Il me semble important de préciser que l'évaluation par trois indicateurs par projets est déjà bien...


