---
author: Laura Fléron
title: A propos

hide:
  - tags
---

# A Propos

- 🚀 : Partie terminée,
- 🚧 : Partie vide,
- 🏗️ : Partie en construction,

## Crédits

Le site est hébergé par la forge de [*l'Association des Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/).

Le site est construit avec [mkdocs](https://www.mkdocs.org/) et en particulier [mkdocs-material](https://squidfunk.github.io/mkdocs-material/).

## Sources

[Lien vers mon Repo](https://forge.aeif.fr/lfleron/git).

## Version

|Date       |Objet                      |
|:---------:|:-------------------------:|
|||
|25/11/23|HTTPS vs SSH|
|29/06/23 | premier dépot|

## Auteure

Laura Fléron, NSI, Académie de Reims.