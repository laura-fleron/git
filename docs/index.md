---
author: Laura Fléron
title: Index
---

# Développer avec Git

Ce cours est composé de pages expliquant l'intérêt et l'usage d'un serveur Git.

!!! warning "Accédez à la page d'explication de la formation NSI du 07/12/2023"

     [Installation du client git, VSCodium et de leurs usages](./logiciel_git/Git_SSH.md#formation_nsi)  


??? success "Retrouvez ce site sur votre smartphone"

    ![image](qr-code.png){: .center width=30%}

??? tip "Trouver une rubrique particulière"
        
    Les rubriques sont classées par *tags*, ce qui vous permet de trouver rapidement un élément particulier.

    Pour cela rendez-vous dans la section [**Tags**](tags.md)
