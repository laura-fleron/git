---
author: Laura Fléron
title: Git

tags:
 - Clone
 - Commit
 - Branch
 - Merge
 - Pull
 - Revert
 - Add
 - Push
 - Issue
 - MkDocs
---

# Git 🏗️

Voici une petite vidéo en guise d'introduction :

<div style="display: flex; justify-content: center; align-items: center;">
    <video controls width="480" muted>
        <!--- Attention au chemin ! lors de la generation du site par mkdocs --->
        <source src="../videos/GIT_cest_quoi.mp4" type="video/mp4">
    </video>
</div>

<!--- ![type:video](GIT_cest_quoi.mp4){: .center width=30%} "https://lfleron.forge.aeif.fr/git/git/GIT_cest_quoi.mp4" --->

Il n'est pas question ici de construire un guide complet de l'usage de Git. Sa documentation, des *tutos* en ligne le font déjà très bien.
Il est plutôt question de voir comment son usage pourrait s'articuler dans nos classes en informatique, et ce qu'il peut nous apporter.

Quand on découvre un outil et qu'il parait vraiment bien, l'idée de tout prendre et tout appliquer est tentant. Mais à vouloir tout faire, on finit pas ne rien conclure.

Git est un outil puissant reconnu par de nombreux dev. Je vais tenter de proposer un cadre d'emploi pour chaque fonction de Git.

Tout d'abord listons les cas d'emplois possibles :

## Le logiciel Git

Git est un logiciel local de controle de versions d'un projet *(VCS : Version Control System)* qui permet à des personnes, développeurs de sauvegarder l'historique de versions de leurs projets, mais il peut également être utilisé pour gérer des versions de documents et d'autres types de fichiers.

GitHub, Gitlab sont des plateformes web qui intègrent les fonctionnalités de contrôle de versions de Git. Vous pouvez également configurer votre propre serveur Git autonome (Les Facultés ont leurs propres serveurs ainsi que certaines académies), *Cf : Liens*.

### Les principales actions disponibles dans Git

🚨 les oubliés :
- Chekout
- rebase
- reset
- status
- autres ??

<a id="clone"></a>
??? abstract "Démarrer un projet à plusieurs : **clone** 🚧"

    === "Définition"
         ...
    
    === "Comment faire"
         ...
    
<a id="commit"></a>
??? abstract "Gérer vos modifications dans un projet : **commit**"

    === "Définition"
    
         Git permet de suivre l'historique complet des modifications apportées à un projet. Chaque modification est enregistrée sous forme de "commit" avec un message explicatif, ce qui facilite la compréhension de l'évolution du projet.

    === "Comment faire" 

         Voici les étapes pour réaliser un commit dans Git :

         1. **Assurez-vous d'être dans le bon répertoire** : Ouvrez votre terminal ou votre interface en ligne de commande, et assurez-vous que vous êtes dans le répertoire de travail du projet Git pour lequel vous souhaitez effectuer un commit.

         2. **Vérifiez l'état du dépôt** : Vous pouvez utiliser la commande `git status` pour vérifier l'état actuel de votre dépôt Git. Elle vous montrera les fichiers modifiés, les fichiers ajoutés, etc.

         3. **Ajoutez les fichiers à la zone de staging** : Utilisez la commande `git add` pour ajouter les fichiers que vous souhaitez inclure dans le commit à la zone de staging. Par exemple, pour ajouter tous les fichiers modifiés, vous pouvez utiliser `git add .` ou pour ajouter des fichiers spécifiques, utilisez `git add nom-du-fichier`.

         4. **Vérifiez à nouveau l'état de la zone de staging** : Vous pouvez utiliser `git status` à nouveau pour vérifier que les fichiers que vous voulez committer sont bien dans la zone de staging.

         5. **Effectuez le commit** : Utilisez la commande `git commit` pour créer un commit avec les fichiers de la zone de staging. Vous pouvez spécifier un message de commit pour expliquer les modifications apportées en utilisant l'option `-m`, par exemple : `git commit -m "Message de commit ici"`.

         6. **Confirmez le commit** : Une fois que vous avez saisi le message de commit, appuyez sur la touche "Entrée". Le commit sera créé avec le message que vous avez spécifié.

         7. **Vérifiez l'historique des commits** : Vous pouvez utiliser `git log` pour voir l'historique des commits et vérifier que votre commit a été ajouté avec succès.

         C'est tout ! Vous avez maintenant créé un commit dans Git. Ce commit est une étape enregistrée de l'historique de votre projet, ce qui vous permet de revenir en arrière ou de collaborer avec d'autres personnes sur le code.

<a id="branch"></a>
??? abstract "Créer des versions différentes d'un projet : **branch**"

    === "Définition"
    
         Git facilite la collaboration entre plusieurs personnes sur un même projet. Plusieurs développeurs peuvent travailler sur des branches distinctes, puis fusionner leurs modifications une fois qu'elles sont prêtes.

         Git offre la possibilité de créer des branches, des copies isolées du code source principal. Cela permet aux développeurs de travailler sur des fonctionnalités ou des correctifs de bugs sans perturber le code principal. Les branches peuvent ensuite être fusionnées lorsque le travail est terminé.

    === "Comment faire"

         Voici les étapes pour créer une nouvelle branche dans Git, effectuer des modifications sur cette branche :

         **Création d'une nouvelle branche :**

         1. **Vérifiez la branche actuelle** : Avant de créer une nouvelle branche, assurez-vous d'être sur la branche à partir de laquelle vous souhaitez créer la nouvelle branche (par exemple, la branche principale). Vous pouvez utiliser `git branch` pour afficher la liste des branches et voir sur laquelle vous vous trouvez actuellement (la branche active sera marquée par un astérisque `*`).

         2. **Créez une nouvelle branche** : Utilisez la commande `git branch` suivie du nom de la nouvelle branche que vous souhaitez créer. Par exemple : `git branch ma-nouvelle-branche`.

         3. **Basculez sur la nouvelle branche** : Utilisez la commande `git checkout` pour basculer sur la nouvelle branche que vous venez de créer. Par exemple : `git checkout ma-nouvelle-branche`. Vous pouvez également créer et basculer sur la nouvelle branche en une seule commande avec `git checkout -b ma-nouvelle-branche`.

         **Effectuer des modifications et valider sur la nouvelle branche :**

         4. **Effectuez des modifications** : Apportez les modifications nécessaires à votre projet sur la nouvelle branche. Vous pouvez ajouter, modifier ou supprimer des fichiers, selon vos besoins.

         5. **Ajoutez et committez les modifications** : Utilisez `git add` pour ajouter les fichiers modifiés à la zone de staging, puis `git commit` pour créer un commit avec vos modifications.

<a id="merge"></a>
??? abstract "Ajouter une version d'un projet au dépôt principal : **merge**"

    === "Définition"
    
         Git facilite la collaboration entre plusieurs personnes sur un même projet. Plusieurs développeurs peuvent travailler sur des branches distinctes, puis fusionner leurs modifications une fois qu'elles sont prêtes.

         Git offre la possibilité *fusionner* des versions différente d'un projet. Les branches sont fusionnées lorsque le travail est terminé.

    === "Comment faire" 
         
         Voici les étapes pour fusionner ces modifications dans une autre branche (par exemple, la branche principale) :

         **Fusionner la nouvelle branche :**

         1. **Retournez à la branche de destination** : Pour fusionner les modifications de votre nouvelle branche dans une autre branche (par exemple, la branche principale), utilisez `git checkout` pour retourner à la branche de destination. Par exemple : `git checkout branche-principale`.

         2. **Fusionnez la nouvelle branche** : Utilisez `git merge` pour fusionner la nouvelle branche dans la branche de destination. Par exemple : `git merge ma-nouvelle-branche`. Assurez-vous d'être sur la branche de destination lorsque vous exécutez cette commande.

         3. **Résolvez les conflits (si nécessaire)** : Si des conflits surviennent lors de la fusion, vous devrez les résoudre manuellement en éditant les fichiers en conflit, en utilisant `git add` pour marquer les fichiers comme résolus, puis en exécutant `git commit` pour finaliser la fusion.

         4. **Validez la fusion** : Une fois les conflits résolus (si nécessaire), le commit de fusion sera créé automatiquement. Vous pouvez ajouter un message de fusion si vous le souhaitez.

         Maintenant, vos modifications de la nouvelle branche ont été fusionnées dans la branche de destination (par exemple, la branche principale). Vous pouvez continuer à travailler sur d'autres fonctionnalités ou corrections de bugs en créant de nouvelles branches et en répétant le processus si nécessaire.

<a id="clone"></a>
??? abstract "Gérer des version : **clone** 🚧"

    === "Définition"
         Git offre la possibilité de créer des branches, des copies isolées du code source principal. Cela permet aux développeurs de travailler sur des fonctionnalités ou des correctifs de bugs sans perturber le code principal. Les branches peuvent ensuite être fusionnées lorsque le travail est terminé.
    
    === "Comment faire"
         ...

<a id="111"></a>
??? abstract "Annuler des versions : **...** 🚧"

    === "Définition"
         Si une modification introduit un bug ou un problème, Git permet de revenir à une version antérieure du projet en toute simplicité.
    
    === "Comment faire"
         ...

<a id="pull"></a>
??? abstract "Distribuer le projet : **pull** 🚧"

    === "Définition"

         Git est conçu pour la distribution décentralisée. Chaque développeur a une copie complète de l'historique du projet sur son ordinateur, ce qui rend le processus de développement plus résilient et facilite le travail hors ligne.

    === "Comment faire"
    
         ...

<a id="111"></a>
??? abstract "Résoudre des conflits 🚧"

    === "Définition"
         Lorsque plusieurs personnes modifient le même fichier en même temps, Git permet de résoudre les conflits de manière contrôlée et de fusionner les modifications de manière appropriée.
    
    === "Comment faire"
         ...

<a id="112"></a>
??? abstract "Restaurer le projet à un moment donné: **revert** 🚧"

    === "Définition"
         Git sert également de système de sauvegarde, car il conserve un historique complet du projet. En cas de perte de données, il est possible de restaurer le projet tel qu'il était à un moment donné.
         Annuler un commit non *pusher*
    
    === "Comment faire"
         ...

<a id="issue"></a>
??? abstract "Planifier le projet : **issue** 🚧"

    === "Définition"

         Assure un suivi de la planification des tâches (Méthode des Post-it).

    === "Comment faire"

         ...

<a id="111"></a>
??? abstract "Documenter le projet : **MkDocs** 🚧"

    === "Définition"
         Permet de déployer une documentation par la génération statique de page automatiquement.
    
    === "Comment faire"
         ...

!!! success "En résumé"

    Git est un outil puissant pour la gestion de versions et la collaboration sur des projets de développement de logiciels et d'autres types de projets. Il permet de suivre les modifications, de gérer les branches, de collaborer efficacement et de garantir la cohérence du code source.

    Vaste programme donc 😁.

??? info "Une vidéo plus complète sur GIT"

    <div style="display: flex; justify-content: center; align-items: center;">
      <video controls width="480" muted>
         <!--- Attention au chemin ! lors de la generation du site par mkdocs --->
         <source src="../videos/LES_BASES_DE_GIT.mp4" type="video/mp4">
      </video>
    </div>

    Un premier memo [-> ici <-](../docu/github-git-cheat-sheet.pdf) pourrait être utile.