---
author: Laura Fléron
title: Git en HTTPS

tags:
 - HTTPS
---

# Git - HTTPS 🚀

Configurer Git sur Windows pour utiliser le protocole *HTTPS* est un peu plus simple par rapport au protocole *SSH*. <a href="HTTPSvsSSH.md">Voir partie HTTPS Vs SSH</a>.

Voici les étapes à suivre :

### Configuration initiale

1. **Installation de Git sur windows :** Si vous ne l'avez pas déjà installé, téléchargez et installez Git depuis le site officiel : [Git - Downloads](https://git-scm.com/downloads).

   **Installation de Git sur Linux :**
   Pour Linux git est déjà installé. Ouvrir un terminal et utilisez les commandes Git que nous allons voir plus bas...

   Mais si vous utilisez une distribution Light ne disposant pas de git, vous pouvez l'installer via le gestionnaire de paquets de votre distribution Linux.
   
   Par exemple, sur Ubuntu ou Debian, vous pouvez utiliser :

   ``` bash
   sudo apt update
   sudo apt install git
   ```


2. **Configuration de Git :** Une fois l'installation terminée, ouvrez Git Bash (ou utilisez l'invite de commandes si vous préférez) et configurez votre nom d'utilisateur et votre adresse e-mail en utilisant les commandes suivantes :

   ``` bash
      git config --global user.name "Votre Nom"
      git config --global user.email "votre@email.com"
   ```

### Utilisation de HTTPS pour cloner un dépôt

1. **Obtenir l'URL HTTPS du dépôt :** Allez sur la page du dépôt Git que vous souhaitez cloner et copiez l'URL HTTPS fournie.

![ClonerGit](images/ClonerGit.png)

2. **Clonage du dépôt :** Dans Git Bash ou l'invite de commandes, naviguez vers le répertoire où vous souhaitez cloner le dépôt et utilisez la commande suivante :

   ``` bash
      git clone [https://url-de-votre-depot.git]
   ```

   Remplacez `[https://url-de-votre-depot.git]` par l'URL HTTPS que vous avez copiée.

3. **Authentification :** Lorsque vous effectuez des opérations comme `push` ou `pull`, Git vous demandera votre nom d'utilisateur et votre mot de passe pour authentifier votre accès au dépôt distant.

4. **Mémorisation des informations d'identification :** 
 
   Pour éviter de saisir votre nom d'utilisateur et votre mot de passe à chaque opération, vous pouvez utiliser des solutions comme le gestionnaire d'informations d'identification Git (`git-credential-manager-core`). 

- **Sur une machine Windows**

   Lors de votre demande de clonage, Windows fera surgir une fenêtre vous demandant les paramètres de votre compte, c'est le *Crédential Manager**.

   Une fois vos paramètres d'indentifications fournis, Git ne vous demendera plus de les communiquer.

   **Pour les vérifier :**

   Ouvrir le Gestionnaire d'informations d'identification, tapez Gestionnaire d’informations d’identification dans la zone de recherche de la barre des tâches, puis sélectionnez panneau Gestionnaire d'informations d'identification.

   Sélectionnez informations d’identification Web ou informations d’identification Windows pour accéder aux informations d’identification que vous souhaitez gérer.

??? note "En image"

    Vous pouvez supprimer / modifier les informations des comptes enregistrés ici.

    [Le crédential Manager](images/Credential.png)

- **Sur une machine LINUX.**

   Pour l'activer, exécutez :

   ```
   git config --global credential.helper manager-core
   ```
   Cela peut varier selon la version de Git que vous utilisez.
 
   Git dispose d’un système de gestion d’identifiants qui peut faciliter cette gestion.
   
   Git propose de base quelques options :

   - Par défaut, rien n’est mis en cache. Toutes les connexions vous demanderont votre nom d’utilisateur et votre mot de passe.

   - Le mode « cache » conserve en mémoire les identifiants pendant un certain temps. Aucun mot de passe n’est stocké sur le disque et les identifiants sont oubliés après 15 minutes. On peut changer le timing avec ` git config --global credential.helper cache --timeout 3000` soit 50 minutes.

   - Le mode « store » sauvegarde les identifiants dans un fichier texte simple sur le disque, et celui-ci n’expire jamais. Ceci signifie que tant que vous ne changerez pas votre mot de passe sur le serveur Git, vous n’aurez plus à entrer votre mot de passe. Le défaut de cette approche est que vos mots de passe sont stockés en clair dans un fichier texte dans votre répertoire personnel.

   Vous pouvez choisir une de ces méthodes en paramétrant une valeur de configuration Git :

   `$ git config --global credential.helper cache`

   ou 

   `$ git config --global credential.helper store`

   Dans ce cas, votre identifiant et mot de passe sont stockés dans le *home* et sont visibles par `nano ~/.git-credentials`

   Les paramètres de configurations de Git sont au même niveau visible par `nano ~/.gitconfig` 

### Opérations habituelles

Une fois que vous avez cloné un dépôt avec HTTPS, vous pouvez effectuer des opérations Git normales (comme `git add`, `git commit`, `git push`, etc.) en utilisant les commandes habituelles.
