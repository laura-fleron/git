---
author: Laura Fléron
title: Git en SSH

tags:
 - Jetons d'accès
 - Token
---

# Git - token 🏗️

Pour cloner un dépôt Git en utilisant un jeton pour l'authentification.

Les jetons, également appelés "tokens" en anglais, sont des clés d'authentification utilisées pour accéder aux serveurs Git de manière sécurisée. Ils permettent à un utilisateur ou à une application d'authentifier son identité sans avoir à saisir de nom d'utilisateur et de mot de passe à chaque interaction avec le serveur Git. Ces jetons sont généralement utilisés pour accéder à distance à un dépôt Git, que ce soit pour cloner, pousser ou tirer des modifications.

Les jetons et les clés SSH sont deux méthodes d'authentification utilisées pour sécuriser l'accès aux serveurs Git, mais ils fonctionnent différemment :

Les jetons sont des chaînes de caractères générées par le serveur Git et peuvent être utilisés comme des "jetons d'accès" temporaires ou permanents pour interagir avec le serveur Git. Ils sont plus faciles à gérer car ils peuvent être révoqués ou régénérés sans affecter d'autres aspects de la sécurité.



Suivre ces étapes :

1. **Générer un jeton d'accès** :
   - Allez sur la plateforme Git où se trouve le dépôt.
   - Accédez aux paramètres de sécurité ou aux paramètres du compte pour générer un jeton d'accès.
   - Générez un nouveau jeton en spécifiant les autorisations nécessaires pour cloner des dépôts.
   
2. **Utiliser le jeton lors du clonage** :
   - Sur votre terminal, utilisez la commande `git clone` en spécifiant l'URL du dépôt.
   - Lorsque vous êtes invité à vous authentifier, utilisez le jeton comme nom d'utilisateur avec un champ vide pour le mot de passe. Par exemple :

     ```bash
     git clone https://jeton@url_du_depot.git
     ```
   Remplacez `jeton` par le jeton que vous avez généré et `url_du_depot.git` par l'URL de votre dépôt.

Cela vous permettra de cloner le dépôt en utilisant le jeton comme méthode d'authentification au lieu d'un nom d'utilisateur et d'un mot de passe.

!!! info "EN résumé"

    En résumé, les jetons sont plus simples à gérer et peuvent être révoqués facilement, tandis que les clés SSH offrent une sécurité plus forte mais nécessitent une gestion plus attentive des clés.