---
author: Laura Fléron
title: HTTPS vs SSH

tags:
 - Comparaison HTTPS / SSH
---

<a id="httpsvsssh"></a>

# HTTPS vs SSH 🚀

Voici une liste d'avantages et d'inconvénients entre un clone de dépôt Git utilisant HTTPS et un utilisant SSH :

### Clone avec HTTPS :

#### Avantages :
1. **Facilité d'utilisation :** Configuration plus simple pour les débutants. Aucune clé SSH à générer ou à gérer.
  
2. **Compatibilité réseau :** HTTPS passe généralement à travers les pare-feu et les proxies plus facilement que SSH, ce qui peut être utile dans certains environnements restreints.

3. **Authentification avec nom d'utilisateur/mot de passe :** L'authentification se fait souvent via nom d'utilisateur et mot de passe, ce qui est familier pour beaucoup d'utilisateurs.

4. **Accès public simplifié :** Utile pour les dépôts publics où l'authentification SSH n'est pas nécessaire.

5. **Documentation accessible :** L'usage d'HTTPS est souvent mieux documenté par les services d'hébergement de dépôts Git pour les débutants.

#### Inconvénients :
1. **Authentification récurrente :** Nécessite de saisir le nom d'utilisateur et le mot de passe à chaque interaction avec le dépôt distant, sauf si des outils de gestion de mot de passe sont utilisés.

2. **Moins sécurisé :** Risque de compromission de mot de passe si celui-ci est faible ou exposé.

3. **Complexité pour les accès privés :** L'authentification avec des dépôts privés peut nécessiter l'usage de tokens d'accès ou d'autres moyens d'authentification.

4. **Gestion des clés :** Pour les dépôts privés, HTTPS peut exiger la gestion de clés de sécurité spécifiques pour l'authentification, ajoutant de la complexité.

5. **Pas d'interactivité avec l'authentification :** Les demandes d'authentification (comme l'usage d'une passphrase pour une clé) ne sont pas aussi interactives que celles de SSH.

### Clone avec SSH :

#### Avantages :
1. **Sécurité renforcée :** Utilisation de clés cryptographiques publiques/privées pour l'authentification, offrant une sécurité plus forte.

2. **Authentification sans mot de passe :** Pas besoin de saisir un mot de passe à chaque interaction avec le dépôt distant une fois la clé SSH configurée.

3. **Facilité d'accès pour dépôts privés :** Idéal pour les dépôts privés nécessitant une authentification sécurisée.

4. **Interactivité dans l'authentification :** Permet l'usage de passphrases pour sécuriser la clé SSH, offrant une couche supplémentaire de sécurité.

5. **Performance :** L'utilisation de SSH peut parfois être plus rapide pour les opérations Git.

#### Inconvénients :
1. **Configuration plus complexe :** Génération et gestion des clés SSH nécessaires, ce qui peut être complexe pour les utilisateurs débutants.

2. **Problèmes de pare-feu :** Certains pare-feu peuvent bloquer les connexions SSH sortantes, rendant la configuration difficile dans certains environnements réseau.

3. **Moins compatible avec certains environnements réseau restreints :** SSH peut être bloqué dans certains environnements réseau stricts.

4. **Gestion des clés :** La gestion des clés SSH peut être fastidieuse pour les utilisateurs qui ne sont pas familiers avec les pratiques de sécurité.

5. **Documentation parfois moins accessible :** La configuration SSH peut être moins bien documentée pour les débutants que l'utilisation d'HTTPS.

Le choix entre HTTPS et SSH dépend souvent du contexte spécifique du projet, des exigences de sécurité et des préférences de l'utilisateur en matière de configuration et de gestion des clés d'authentification.