---
author: Laura Fléron
title: Git en SSH

tags:
 - SSH
---

# Git - SSH 🏗️

Configurer Git sur Windows pour utiliser le protocole *SSH*.

Nous avons vu comment manipuler les dépots par HTTPS, voyons maintenant la configuration pour employer le protocole SSH

Voici les étapes à suivre :

### Configuration initiale et installation de Git

1. **Installation de Git :** Si vous ne l'avez pas déjà installé, téléchargez et installez Git depuis le site officiel : [Git - Downloads](https://git-scm.com/downloads).

2. **Configuration de Git :** Une fois l'installation terminée, ouvrez Git Bash (ou utilisez l'invite de commandes si vous préférez) et configurez votre nom d'utilisateur et votre adresse e-mail en utilisant les commandes suivantes :

Si ce n'est pas encore fait...

   ```
   git config --global user.name "Votre Nom"
   git config --global user.email "votre@email.com"
   ```

### génération des clé SSH

Grâce à SSH, notre machine locale va pouvoir établir une connexion sécurisée à GitLab pour télécharger et pousser les données de notre projet Git. Ainsi, pour se connecter à GitLab depuis Git, l'authentification s'effectuera par un échange de clés plutôt qu'un couple "identifiant / mot de passe". GitLab accepte différents types de clés SSH, notamment les clés ED25519 (à prioriser vis-à-vis de RSA !).

Le client SSH est préinstallé sur plusieurs versions de Windows, notamment Windows 10 et Windows 11, ainsi que Linux et macOS. À partir d'une console ouverte sur ma machine Windows, on peut voir facilement quelle est la version d'OpenSSH installée sur ma machine :

#### Comment utiliser le client SSH sous Windows ?

Dans la zone de recherche de windows, rechercher l'invite de commande et faites :

```texte
C:\Users\Travail>ssh -V
OpenSSH_for_Windows_8.6p1, LibreSSL 3.4.3
```
Qui dit authentification par clés, dit que nous devons disposer d'une paire de clés, à savoir une clé privée qui restera sur notre machine locale, et une clé publique que l'on va déclarer par la suite côté GitLab. Si vous avez déjà utilisé l'authentification par clé SSH sur votre machine, ce n'est pas nécessaire de régénérer une nouvelle paire de clés (même s'il est possible d'avoir plusieurs paires de clés avec des noms différents). Sinon, il faut le faire dès maintenant avec l'utilitaire ssh-keygen.exe, inclus avec le client SSH.

**Comment savoir si vous avez déjà des clés SSH ? **

Pour cela, accédez au répertoire ".ssh" situé dans votre profil utilisateur. Sous Windows, et pour l'utilisateur "Travail", cela donne :

```texte
C:\Users\Travail\.ssh

Répertoire de C:\Users\Travail\.ssh

28/11/2023  19:25    <DIR>          .
29/11/2023  19:39    <DIR>          ..
28/11/2023  20:01               209 known_hosts
28/11/2023  19:25             2 655 ed258
28/11/2023  19:25               578 ed258.pub
               3 fichier(s)            3 442 octets
               2 Rép(s)  153 994 739 712 octets libres
```
Nous voyons deux fichiers :

- la **clé privée, lf2023**
- la **clé publique, lf2023.pub**

Nous avions déjà généré une paire de clé.

#### Si je n'ai pas encore générer de clé SSH.

!!! danger "Attention !"

    Lorsque vous devez copier ou télécharger votre clé publique SSH, assurez-vous de ne pas copier ou télécharger accidentellement votre clé privée à la place.
    
Si vous n'avez pas de paire de clés SSH existante, générer une nouvelle paire.

Pour cela faire, ouvrez un terminal, puis :

1. Exécutez `ssh-keygen -t`suivi du type de clé et d'un commentaire facultatif. Ce commentaire sera inclu dans le .pub du fichier qui sera créé. Vous pouvez utiliser une adresse e-mail pour le commentaire...

2. Acceptez le nom de fichier et le répertoire suggérés.

3. Préciser un mot de passe, ou vous pouvez laisser vide.

4. Une confirmation est affichée, y compris des informations sur l'endroit où vos fichiers sont 

Par exemple, pour ED25519:

```bash
    ssh-keygen -t ed25519 -C "Laura"
    Generating public/private ed25519 key pair.
    Enter file in which to save the key (/home/laura/.ssh/id_ed25519): 
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    Your identification has been saved in /home/laura/.ssh/id_ed25519
    Your public key has been saved in /home/laura/.ssh/id_ed25519.pub
    The key fingerprint is:
    SHA256:s**************************************0 Laura
    The key\'s randomart image is:
    +--[ED25519 256]--+
    |    .. .+o=o.    |
    |   . ..o B.o     |
    |  ..+.. E =      |
    |   =o. + B o     |
    |  o.+ + S = .    |
    | ..o + . + +     |
    |... o .   .      |
    |o=.o             |
    |X++.             |
    +----[SHA256]-----+
```

Le répertoire `.ssh` contient maintenant deux fichiers : `ed25519` et `ed25519.pub`

Copiez la clé publique pour la renseigner dans le serveur Git.

**Avec Linux (nécessite le xclippaquet)**

`xclip -sel clip < ~/.ssh/id_ed25519.pub`

Sinon ouvrez le fichier avec l'intruction `cat id_ed25519.pub` et copiez la ligne.

**Avec Git Bash sur Windows**

`cat ~/.ssh/id_ed25519.pub | clip`

### Sur le site web de Gitlab.com ou autre serveur Git

1. Connectez-vous à GitLab.
2. Sur la barre latérale gauche, sélectionnez votre avatar.
3. Sélectionnez *préférence* du profil.
4. Sur la barre latérale gauche, sélectionnez SSH Clés.
5. Sélectionnez *Ajouter une nouvelle clé*.
6. Dans la zone Clé, collez le contenu de votre clé publique. Le format, la valeur de la clé et le commentaire
7. Dans la zone de titre, tapez une description,
8. Sélectionnez le type d'utilisation de la clé. Il peut être utilisé soit pour l'authentication ou pour le mot de passe ou les deux.
9. La date d'expiration. Par défaut un an.

### Connectez votre client Git au serveur

Ouvrez un terminal et exécutez la commande `ssh -T git@gitlab.example.com`, en remplaçant gitlab.example.com avec votre URL de l'instance GitLab:

Pour retrouver votre URL, rendez-vous dans votre projet et copiez la dans la zone en haut à droite dans la zone de sélection *Clone*

Pour cloner un dépôt Git en utilisant un lien SSH :

1. **Obtenir l'URL SSH du dépôt :** Sur la page du dépôt Git que vous souhaitez cloner (par exemple, sur GitHub, GitLab, Bitbucket), cherchez l'option pour cloner en utilisant SSH. Copiez l'URL SSH fournie.

![ClonerGit](images/ClonerGit.png)

2. **Ouvrir un terminal :** Ouvrez un terminal sur votre système d'exploitation (Linux, macOS, Windows avec Git Bash).

3. **Clonage du dépôt :** Utilisez la commande `git clone` suivie de l'URL SSH du dépôt que vous avez copiée. Par exemple :
   
   ```bash
   git clone git@github.com:utilisateur/nom-depot.git
   ```
   Remplacez `git@github.com:utilisateur/nom-depot.git` par l'URL SSH du dépôt que vous souhaitez cloner.

Si c'est la première fois que vous vous connectez, vous devez vérifier l'authenticité de l'hôte GitLab.

Répondez par *yes*, Puis indiquez le mot de passe renseigné lors de la création des clés, si vous en avez indiqué un.

**Si le message de bienvenue n'apparaît pas**, vous pouvez dépanner en utilisant le paramètre verbeux, `ssh -Tvvv git@gitlab.example.com`

### Pour les serveurs Git scolaires.

Le port SSH par défaut est le 22. Il n'y a rien à faire.

Renseignez vous auprès de votre fournisseur si un autre port a été configuré. L'académie du Grand Est propose un serveur accessible sur le port 2022. Il faudra dans ce cas faire une redirection de ports (TCP) sur votre routeur personnel ou dans votre établissement.

Pour les serveurs de l'AEIF, gitlab, github il n'y a rien a faire.

<a id="formation_nsi"></a>
---
## Pour plus d'informations sur l'installation du client Git avec le serveur du grand Est
---

[Cliquer ici pour voir ce tutoriel en vidéo](https://tube-sciences-technologies.apps.education.fr/w/cokHcURA5TUhn9ZB4uVxRJ)

### Installation de Git

1. Télécharger Git : [https://git-scm.com/download/win](https://git-scm.com/download/win) en choisissant `64-bit Git for Windows Setup`.
2. Installer ce logiciel.

### Installation de Python

1. Ouvrir le `Microsoft Store` et installer la dernière version de l'interpréteur `Python` [la version `Python 3.11` de `Python Software Foundation` au jour où j'écris ce tutoriel] (environ 40 Mo).
2. Installer ce logiciel.

### Installation de VSCodium

1. Se rendre sur le site [https://vscodium.com](https://vscodium.com)
2. Cliquer sur le gros bouton bleu `Download latest release`
3. Dans la page qui s'ouvre, dans la partie `Assets`, cliquer en bas sur `Show all 116 assets` puis choisissez le fichier : `VSCodiumUserSetup-x64-1.82.0.23250.exe` (le numero `1.82.0.23250` peut être différent).
4. Attendre que le téléchargement se termine (le fichier pèse 86,1 Mo) puis exécuter le fichier. Un message d'avertissement apparaît :

> Windows a protégé votre ordinateur
> Microsoft Defender SmartScreen a empêché le démarrage d’une application non reconnue. L’exécution de cette application peut mettre votre ordinateur en danger.
> 
> Informations complémentaires

5. Cliquer sur "Informations complémentaires" puis sur "Exécuter quand même".
6. Poursuivre l'installation du logiciel en choisissant les options appropriées pour votre ordinateur.

#### Première utilisation de VSCodium

1. Ouvrir VSCodium
2. Choisir le thème souhaité en cliquant dessus (thème clair ou thème sombre).

```
Pour mettre VSCodium en français
1. Menu `View>Command Palet` (ou `Ctrl+Maj+P`).
2. Saisir `Configure Display Language` dans la zone de recherche qui s'est ouverte, puis valider.
3. Choisir `français` puis Cliquez sur le bouton `Restart` qui est alors proposé.
```

3. A gauche, dans la rubrique "Extension" (ou `Ctrl+Maj+X`), installer l'extension Python (de l'éditeur `ms-python` avec comme descriptif : `IntelliSense (Pylance), Linting, Debugging (multi-threaded, remote), code formatting, ...`)
4. Quitter tous les onglets en cliquant sur leur croix, en haut.
5. Dans Windows, créez un dossier de travail, par exemple `Documents\NSI\testVSCodium\`
6. Dans VSCodium, à gauche, dans la rubrique "Explorateur" (ou `Ctrl+Maj+E`), le message "Aucun dossier ouvert" devrait apparaître. Cliquer sur le bouton `Ouvrir le dossier`. Choisir dans votre dossier de travail : `Documents\NSI\testVSCodium\` puis cliquer sur le bouton `Ajouter` pour valider.
7. Un message apparaît :

> Faites-vous confiance aux auteurs des fichiers de ce dossier ?
> Vous ajoutez à un espace de travail approuvé des fichiers non approuvés pour l’instant. Faites-vous confiance aux auteurs de ces nouveaux fichiers ?
> 
> Oui Non

8. Cliquer sur `Oui`.
9. Aller dans le menu `Affichage>Palette de commandes...` (ou `Ctrl+Maj+P`).
10. Saisir `Python: Select Interpreter` dans la zone de recherche qui s'est ouverte, puis valider.
11. Choisir à présent l'interpréteur (ex : `Python 3.11`)
12. Menu `Fichier>Nouveau fichier...`
13. Saisir `main.py` puis appuyer sur la touche `Entrée`.
14. Choisir toujours le même dossier, par exemple : `Documents\NSI\testVSCodium\`puis valider.
15. Saisir le code source d'un premier programme simple, comme : `print("Bonjour !")` puis cliquer sur le bouton d'exécution (de forme triangulaire : `Run Python File`) en haut à droite. Cela devrait afficher en bas de l'écran le Terminal, avec le texte "Bonjour !".

<div class="alert alert-info" style="border-left: 15px solid red">
    <h3>La suite de la démarche, ci-dessous, est faite en présentiel lors du stage.</h3>
</div>

<a id="formation_nsi"></a>
## Accès à votre compte sur Gitlab Grand Est

1. Se rendre sur [https://gitlab.lycees.grandest.fr/](https://gitlab.lycees.grandest.fr/)
2. Cliquer sur "Mot de passe oublié"
3. Saisir son adresse mail académique
4. Ouvrir dans sa boîte mail académique le message `Reset password instructions`
5. Cliquer sur le lien : `Réinitialiser le mot de passe`
6. Saisir un mot de passe suffisamment élaboré et le confirmer

### Accès SSH - Générer une clé

Pour obtenir l’accès SSH, il faut se créer une paire de clé privée / clé publique :

1. Dans VSCodium, aller dans le menu `Terminal` > `Nouveau terminal` 
2. Dans la partie `Terminal` qui s'est ouverte en bas de la fenêtre, entrer : 
```
ssh-keygen -t rsa -b 2048 -C "clé SSH pour Gitlab Grand Est"
```
(appuyer au total 3 fois sur entrée pour ne pas changer le nom des fichiers, ne pas indiquer de passphrase et confirmer)
3. Ouvrir la clé publique avec un bloc-notes : ```C:\Users\Michael\.ssh\id_rsa.pub``` (A adapter selon votre configuration)
4. Copier tout le texte du bloc-notes (commençant par “ssh-rsa” ) et le coller dans la zone de saisie “Clé” de la page web : [https://gitlab.lycees.grandest.fr/-/profile/keys](https://gitlab.lycees.grandest.fr/-/profile/keys) puis cliquer sur le bouton “Ajouter une clé” en bas de la page.

### Cloner un projet

1. Se placer dans le dossier souhaité, puis copier son chemin absolu, par exemple : 
```C:\Users\Michael\Documents\NSI```      (A adapter selon votre configuration)
2. Dans la partie `Terminal` de VSCodium, entrer, en adaptant le chemin absolu et les zones NOM, Prénom et e-mail : 
```
cd "C:\Users\Michael\Documents\NSI"
```
```
git config --global user.name "Votre NOM Prénom"
git config --global user.email "Votre e-mail"
```

3. Aller sur l’instance web : [https://gitlab.lycees.grandest.fr/](https://gitlab.lycees.grandest.fr/) puis dans le projet à cloner
4. Dans le `Terminal` de VSCodium, commencer à taper `git clone ` sans valider
5. Dans l'instance web, copier l'adresse SSH du projet en cliquant sur la flèche à droite du bouton bleu `cloner` : 
```
ssh://git@gitlab.lycees.grandest.fr:2022/___________/_________.git
```
6. Coller dans le terminal de VSCodium (en faisant un clic droit pour coller) cette adresse pour avoir la commande :

```
git clone ssh://git@gitlab.lycees.grandest.fr:2022/___________/_________.git
```

### Collaborer à un projet

En ligne de commandes - bash, après s'être placé dans le dossier du dépôt,
Faire des “pull”
```
git pull 
```
Faire des “push”
```
git add . 
git commit -m “votre commentaire”
git push
```

### Créer un projet

On peut créer un nouveau projet depuis l’instance web : [https://gitlab.lycees.grandest.fr/projects/new#blank_project](https://gitlab.lycees.grandest.fr/projects/new#blank_project)

Puis on peut y inviter des membres en allant dans le projet > Gestion > Membres > Inviter des membres.

Lorsque vous utiliserez GitLab, laissez bien les élèves avec le rôle **"Developer"** afin qu'ils ne puissent pas voir la liste des autres élèves de la région (RGPD).

### Les quotas d’espace disque

* Ne pas détourner l’usage de ce GitLab (par exemple en faisant un « Cloud » personnel ou un « Cloud » commun avec leurs élèves). L'espace disque pour tout Gitlab Grand Est est de 1 To.
* Pas de persistance des données d’une année sur l’autre.

### Gérer les tâches et les objectifs d'un projet

#### Les jalons (milestones)

Ce sont les étapes principale d'un projet qui permettent de définir des objectifs spécifiques à atteindre à des moments précis.
Ils facilitent la planification, la communication et permettent de définir
des objectifs réalisables.
Cela aide les élèves à rester concentrés sur les tâches à accomplir et à évaluer
régulièrement l'avancement du projet.

### Résoudre un conflit de synchronisation

#### Situation

Plusieurs élèves travaillent dans un même groupe et éditent le même fichier localement. 
1. Le premier élève fait un ```push```. 
2. Le second ne peut donc plus faire son ```push```. Il doit d’abord ```pull```.
3. Lorsque les deux élèves ont modifié les mêmes parties de code, un conflit de fusion est à résoudre par le second élève. Il est alors plus simple de résoudre les conflits dans un client git muni d’une interface graphique, comme VSCodium. L'écran se scinde en 3 zones : à gauche la version distante, à droite la version locale, en bas le résultat souhaité (soit en acceptant la partie gauche, soit la droite, soit en mélangeant les deux à sa convenance)


### Les quotas d’espace disque
* Ne pas détourner l’usage de ce GitLab (par exemple en faisant un « Cloud » personnel ou un « Cloud » commun avec leurs élèves). L'espace de stockage maximal pour une classe de NSI est de 2 Go.
* Pas de persistance des données d’une année sur l’autre.

### Les clients graphiques

Pour évaluer et suivre les projets des élèves, le client **Gitextensions** nous a paru approprié:
[https://gitextensions.github.io/](https://gitextensions.github.io/) 

Sinon, une liste complète de clients graphiques est disponible ici :
[https://git-scm.com/download/gui/windows](https://git-scm.com/download/gui/windows) 
mais certains ne fonctionnent pas avec gitlab.

### Contacter les membres de la DRANE référents pour GitLab Grand Est :

* Pascal THERESE : Pascal.Therese@ac-reims.fr
* Michael BALANDIER : Michael-Lionel.Balandier@ac-reims.fr
